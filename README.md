# MPlant Test Runner
This is an experimental test runner for MPlant.

## How to use
### Setup
At present, this test suite expects an MPlant directory to exist as a sibling folder to the test respository.  The applicable MPlant libraries should already be built in their respective folders.
### About
This test tool is a python script that builds and executes small programs contained in yaml files.  Each yaml file indicates which MPlant libraries and headers are required, then contains some number of unit tests. Each unit test is the minimum set of C++ commands that would become part of a main() function.

The test suite takes care of building, compiling, and executing the test with a simpl pass/fail designation for each test that is run.

Advanced features include a debug mode that will also include lines prefixed by DEBUG, and a valgrind mode that looks for memory leaks in the unit tests.
### Usage
```
python MPlantTester.py [-h] [--debug] [--verbose] [--valgrind] [--regex REGEX] file

Unit test runner for MPlant

positional arguments:
  file                  The test yaml file to execute or a directory
                        containing test yaml files

optional arguments:
  -h, --help            show this help message and exit
  --debug, -d           Include debug lines from tests
  --verbose, -v         Verbose output
  --valgrind, -g        Run valgrind
  --regex REGEX, -r REGEX
                        Match the tests to run against an expression
```
