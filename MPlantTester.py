#
# This file is part of the MPlant test runner.
#
# Copyright (c) 2018-2020 Drake Applications
#
# MPlantTester is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MPlantTester is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MPlantTester.  If not, see <https://www.gnu.org/licenses/>.
#

import argparse
import os
import re
import yaml

# Set up program arguments

parser = argparse.ArgumentParser(description='Unit test runner for MPlant')
parser.add_argument('--debug', '-d', action='store_true', help='Include debug lines from tests')
parser.add_argument('--verbose', '-v', action='store_true', help='Verbose output')
parser.add_argument('--valgrind', '-g', action='store_true', help='Run valgrind')
parser.add_argument('--regex', '-r', default='.*', help='Match the tests to run against an expression')
parser.add_argument('file', help='The test yaml file to execute or a directory containing test yaml files')
args = vars(parser.parse_args())

# Main entry point

def main():

    filePath = args['file']

    if os.path.isdir(filePath):
        for listedFile in os.listdir(filePath):
            if listedFile.endswith(".yaml") or listedFile.endswith(".yml"):
                runTestFile(os.path.join(filePath, listedFile))
    elif os.path.isfile(filePath):
        runTestFile(filePath)
    else:
       raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), filePath)

# Build and execute the tests containted in the given file

def runTestFile(testFilePath):
    # The idea is to compile little test programs that are defined
    # by simple test lines in a yaml file.

    regex = re.compile(args['regex'])

    test_definition = None

    with open(testFilePath, 'r') as stream:
        try:
            test_definition = yaml.load(stream)
        except yaml.YAMLError as ex:
            print(ex)
            return -1

    setups = []
    try:
        setups = test_definition["setup"]
    except:
        # No setups, no problem
        pass

    for action in setups:
        os.system(action)

    env_info = test_definition["env"]
    includes = test_definition["includes"]
    tests    = test_definition["tests"]

    for test in tests:
        if regex.search(test["name"]) or regex.search(test["desc"]):
            create_test(includes, test["test"], args["debug"])
            compile_test(env_info);
            execute_test(test["name"], test["desc"], env_info, args["valgrind"])

    cleanups = []
    try:
        cleanups = test_definition["cleanup"]
    except:
        # No cleanups, no problem
        pass

    for action in cleanups:
        os.system(action)

    pass

# Create a C++ program based on the given test instructions

def create_test(includes, test, debug):

    test_data = '';

    if debug:
        test_data = "#include <iostream>\n"

    for inc in includes:
        test_data += "#include " + inc + "\n"

    test_data += "int main(void)" + "\n"
    test_data += "{" + "\n"
    test_data += "  bool pass = false;" + "\n"

    for line in test:
        if line:
            if line.startswith('DEBUG '):
                if debug:
                    line = line[6:]
                else:
                    line = ''
            test_data += '  ' + line + "\n"

    test_data += "  return (pass ? 0 : -1);" + "\n"
    test_data += "}" + "\n"

    if args['verbose']:
        print(test_data + "\n")

    test_file = open("test.cpp", 'w');
    test_file.write(test_data);
    test_file.close();
    

# Compile the given C++ program created earlier

def compile_test(env_info):

    includePath = ''
    libPath = ''
    libs = ''

    for lib in env_info["LIBS"]:
        libs += "-l " + lib + " "

    for lp in env_info["LIBPATHS"]:
        libPath += "-L " + lp + " "

    for inc in env_info["INCPATHS"]:
        includePath += "-I " + inc + " "

    cpp_cmd = "g++ -std=c++11 {} {} {} -o test.bin test.cpp".format(includePath, libPath, libs)

    if args['verbose']:
        print(cpp_cmd + "\n")

    os.system(cpp_cmd)
    pass

# Execute a compiled test

def execute_test(test_name, test_desc, env_info, useValgrind = False):

    ld_lib_path = 'LD_LIBRARY_PATH="'
    valgrind = ''

    for lp in env_info["LIBPATHS"]:
        ld_lib_path += lp + ":"

    ld_lib_path += '"'

    if useValgrind:
        valgrind = 'valgrind --tool=memcheck --leak-check=full'

    os.system("echo -n '{} - {}: '; ({} {} ./test.bin && echo pass) || echo fail".format(test_name, test_desc, ld_lib_path, valgrind));

    if args['verbose']:
        print("\n")

    os.system("rm -rf test.bin test.cpp")
    pass

# Run the program
main()
